package main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.yworks.yfiles.view.GraphComponent;
import com.yworks.yfiles.view.input.GraphEditorInputMode;

import actions.OpenCSVAction;
import actions.SaveXMLAction;

public class Main {

	public Main() {
		// Create the Graph
		GraphComponent graphComponent = new GraphComponent();
		graphComponent.setInputMode(new GraphEditorInputMode());
		graphComponent.setFileIOEnabled(true);

		// Then show it
		JFrame frame = new JFrame("Course Requirements - LNU");
		frame.setSize(1200, 800);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		JToolBar toolbar = new JToolBar();
		toolbar.add(new OpenCSVAction(graphComponent));
		toolbar.add(new SaveXMLAction(graphComponent));
		frame.add(toolbar, BorderLayout.NORTH);

		frame.add(graphComponent, BorderLayout.CENTER);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(Main::new);
	}
}
