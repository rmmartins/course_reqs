package main;

import java.io.File;
import java.io.IOException;

import com.yworks.yfiles.view.GraphComponent;

import actions.OpenCSVAction;
import model.Graph;

public class CmdLine {

	public static void main(String[] args) {
		String jarName = "courseDeps";
		
		if (args.length < 1) {
			System.err.println("Usage: java -jar " + jarName + ".jar file1.csv [file2.csv file3.csv ...]");
			System.err.println("  The results will be saved in XML files with the 'graph' prefix.");
			System.exit(-1);
		}
		
		GraphComponent graphComponent = new GraphComponent();
		
		OpenCSVAction open = new OpenCSVAction(graphComponent);
		
		File[] files = new File[args.length];
		for (int i = 0; i < args.length; ++i) {
			File f = new File(args[i]);
			if (!f.exists()) {
				System.err.println("File '" + args[i] + "' does not exist. Aborting.");
				System.exit(-1);
			}
			files[i] = f;
		}
		
		// TODO Refactor this operation out of the action; use actions only in the GUI
		open.loadCSV(files);
		
		Graph myGraph = open.getMyGraph();
		for (File f : files) {
			String absPath = f.getAbsolutePath();
			String program = absPath.substring(absPath.lastIndexOf("_") + 1).split("\\.")[0];
			myGraph.layout(graphComponent, program);
			// TODO Refactor this into a "SaveXML" class or something (it is also in the SaveXMLAction)
			try {
				graphComponent.exportToGraphML(absPath.substring(0, absPath.lastIndexOf(".")) + ".xml");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		myGraph.layout(graphComponent);
		String path = files[0].getParent();
		try {
			graphComponent.exportToGraphML(path + "/graph_full.xml");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
