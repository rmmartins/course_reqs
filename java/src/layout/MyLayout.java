package layout;

import java.awt.Color;
import java.util.Arrays;

import com.yworks.yfiles.geometry.RectD;
import com.yworks.yfiles.graph.IEdge;
import com.yworks.yfiles.graph.IGraph;
import com.yworks.yfiles.graph.INode;
import com.yworks.yfiles.graph.LayoutUtilities;
import com.yworks.yfiles.graph.styles.IArrow;
import com.yworks.yfiles.graph.styles.PolylineEdgeStyle;
import com.yworks.yfiles.graph.styles.ShapeNodeStyle;
import com.yworks.yfiles.layout.LayoutOrientation;
import com.yworks.yfiles.layout.hierarchic.HierarchicLayout;
import com.yworks.yfiles.layout.hierarchic.ILayerConstraintFactory;
import com.yworks.yfiles.view.GraphComponent;
import com.yworks.yfiles.view.Pen;

import model.Node;

public class MyLayout {
	
	private RectD defaultSize = new RectD(0,0,120,80);
	private RectD smallSize = new RectD(0,0,40,40);
	private RectD mediumSize = new RectD(0,0,90, 40);
	
	// TODO Refactor the 'colors' and 'layerRefs' arrays to initialize with a specific number of years

	private Color[][] colors = {
			{ Color.green, 		Color.green.darker() },
			{ Color.yellow, 		Color.yellow.darker() },
			{ Color.blue, 		Color.blue.darker() },
			{ Color.orange, 		Color.orange.darker() },
			{ Color.magenta, 	Color.magenta.darker() }
	};
	
	// Three layers, two sub-layers, each will have one reference node
	private INode[][] layerRefs = new INode[5][4];
	
	private IGraph graph;
	private HierarchicLayout hl = new HierarchicLayout();
	private ILayerConstraintFactory lcf;
		
	// TODO maybe it makes sense to initialize with the component and just use it to take the graph
	public MyLayout(IGraph graph) {
		this.graph = graph;

		// Make sure we are working with an empty graph
		graph.clear();
		
		lcf = LayoutUtilities.createIncrementalLayerConstraintFactory(hl, graph);
		hl.setLayoutOrientation(LayoutOrientation.LEFT_TO_RIGHT);
	}
	
	public final INode createNode(Node node) {
		INode inode = graph.createNode(defaultSize);
		
//		String label = "";
//		for (String p : node.getPrograms()) {
//			label += p + " ";
//		}
//		label += "\n" + node.getLabel();
		
		String label = node.getLabel();
		
		graph.addLabel(inode, label);
		
		int year = node.getYear(), period = node.getPeriod();
		// If it's not a ghost node
		if (year > -1 && period > -1) {
			// Add layer constraint to node
			if (layerRefs[year-1][period-1] == null) {
				layerRefs[year-1][period-1] = inode; 
			} else {
				lcf.addPlaceNodeInSameLayerConstraint(layerRefs[year-1][period-1], inode);
			}
			
			// Node style
			ShapeNodeStyle style = new ShapeNodeStyle();
			style.setPaint(colors[year-1][period < 3 ? 0 : 1]);
			graph.setStyle(inode, style);
		}
		
		// TODO add a flag/switch for turning these req. nodes off optionally
		if (node.getReqsCs() > 0) {
			INode reqNode = graph.createNode(mediumSize);
			graph.addLabel(reqNode, "CS:" + node.getReqsCs());
			graph.createEdge(reqNode, inode);
		}
		if (node.getReqs2dv() > 0) {
			INode reqNode = graph.createNode(mediumSize);
			graph.addLabel(reqNode, "2DV:" + node.getReqs2dv());
			graph.createEdge(reqNode, inode);
		}
		if (node.getReqsOther() > 0) {
			INode reqNode = graph.createNode(mediumSize);
			graph.addLabel(reqNode, "Oth:" + node.getReqsOther());
			graph.createEdge(reqNode, inode);
		}
		
		String tag = label + "." + year + "." + period;
		inode.setTag(tag);
		
		return inode;
	}
	
	// TODO add overriding method for the default type = ""
	public final void addEdge(INode source, INode target, String type) {		
		IEdge e = graph.createEdge(source, target);
		e.setTag(type);
		if (type.equals("dashed")) {
			PolylineEdgeStyle style = new PolylineEdgeStyle();
			style.setPen(Pen.getRed());
			style.setTargetArrow(IArrow.SIMPLE);
			graph.setStyle(e, style);
		}
	}
	
	public final void layout(GraphComponent graphComponent) {
		for (int i = 0; i < layerRefs.length; ++i) {
			for (int j = 0; j < layerRefs[i].length - 1; ++j) {
				lcf.addPlaceNodeBelowConstraint(layerRefs[i][j], layerRefs[i][j + 1]);
			}
			if (i < layerRefs.length - 1) {
				lcf.addPlaceNodeBelowConstraint(layerRefs[i][layerRefs[i].length - 1], layerRefs[i + 1][0]);
			}
		}
		LayoutUtilities.applyLayout(graphComponent.getGraph(), hl);
		graphComponent.updateContentRect();
		graphComponent.fitContent();
	}

	public INode createPlusNode() {
		INode inode = graph.createNode(smallSize);
		graph.addLabel(inode, "+");
		return inode;
	}

	public INode createNode(Node node, String program) {
		INode inode = createNode(node);
		ShapeNodeStyle style = null;
		// This course is optional in this program
		if (Arrays.asList(node.getOptionals()).contains(program)) {
			if (style == null) {
				style = new ShapeNodeStyle();
			}
			style.setPen(new Pen(Pen.getRed().getPaint(), 5.0));
		}
		// This course is an external dependency in this program
		if (!Arrays.asList(node.getPrograms()).contains(program)) {
			if (style == null) {
				style = new ShapeNodeStyle();
			}
			style.setPen(new Pen(Pen.getBlack().getPaint(), 5.0));
		}
		if (style != null) {
			style.setPaint(((ShapeNodeStyle)inode.getStyle()).getPaint());
			graph.setStyle(inode, style);
		}
		return inode;
	}

}
