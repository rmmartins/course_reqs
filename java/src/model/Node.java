package model;

import java.util.HashSet;
import java.util.Set;

public class Node {
	
	private String label;
	private int year, period;
	private Set<String> programs;
	private Set<String> optional;
	private int reqsCs;
	private int reqs2dv;
	private int reqsOther;
	
	protected Node(String label, int year, int period) {
		this.label = label;
		this.year = year;
		this.period = period;
		this.programs = new HashSet<>();
		this.optional = new HashSet<>();
		this.reqsCs = 0;
	}
	
	public String getLabel() {
		return label;
	}
	
	public int getPeriod() {
		return period;
	}
	
	public int getYear() {
		return year;
	}
	
	public int getReqsCs() {
		return reqsCs;
	}

	public void setReqsCs(int reqsCs) {
		this.reqsCs = reqsCs;
	}

	public int getReqs2dv() {
		return reqs2dv;
	}

	public void setReqs2dv(int reqs2dv) {
		this.reqs2dv = reqs2dv;
	}

	public int getReqsOther() {
		return reqsOther;
	}

	public void setReqsOther(int reqsOther) {
		this.reqsOther = reqsOther;
	}

	public void addProgram(String program) {
		programs.add(program);
	}
	
	public String[] getPrograms() {
		return programs.toArray(new String[0]);
	}
	
	public void addOptional(String program) {
		optional.add(program);
	}
	
	public String[] getOptionals() {
		return optional.toArray(new String[0]);
	}

}
