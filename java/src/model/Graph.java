package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.yworks.yfiles.graph.INode;
import com.yworks.yfiles.view.GraphComponent;

import layout.MyLayout;

public class Graph {
	
	// TODO Maybe I should just keep the map and use the keySey as a node list?
	//		In that case I'd need to keep empty entries for nodes without dependencies.
	private List<Node> nodes;
	private List<Edge> edges;
	
	public Graph() {
		this.nodes = new ArrayList<>();
		this.edges = new ArrayList<>();
	}
	
	public Node createNode(String program, String label, int year, int period, boolean optional) {
		Node n = createNode(label, year, period);
		n.addProgram(program);
		if (optional)
			n.addOptional(program);
		return n;
	}
	
	public Node createNode(String label, int year, int period) {
		if (!label.equals("Elective")) {
			for (Node n : nodes) {
				if (label.equals(n.getLabel())) {
					if (year == n.getYear() && period == n.getPeriod()) {
//						Logger.getGlobal().warning("Node " + label + " already present. Ignoring.");
						return n;
					} else {
						Logger.getGlobal().info("Label " + label + " is already present, but in a different year/period.");
					}
				}
			}
		}
		Node node = new Node(label, year, period);
		nodes.add(node);
		return node;
	}
	
	public Edge createEdge(Node source, Node target, boolean isWeak) {
		Edge e = new Edge(source, target, isWeak);
		edges.add(e);
		return e;
	}
	
	public Edge createEdge(Node[] source, Node target, Boolean[] isWeak) {
		Edge e = new Edge(source, target, isWeak);
		edges.add(e);
		return e;
	}

	// Linear search for now, shouldn't be a problem for small graphs
	public Node[] getNodesBy(String dep, boolean createIfMissing) {
		dep = dep.trim();
		List<Node> nodesRet = new ArrayList<>();
		for (Node n : nodes) {
			if (n.getLabel().equals(dep)) {
				nodesRet.add(n);
			}
		}
		// Node not found = invalid dependency; create a "ghost" node
		if (nodesRet.size() == 0 && createIfMissing) {
			Logger.getGlobal().warning("Invalid dependency: " + dep);
			// Add a "ghost" node as a placeholder
			Node node = createNode(dep, -1, -1);	
			nodesRet.add(node);
		}
		return nodesRet.toArray(new Node[0]);
	}

	public void layout(GraphComponent graphComponent) {
		MyLayout myLayout = new MyLayout(graphComponent.getGraph());
		Map<Node, INode> iNodes = new HashMap<>();
		for (Node node : nodes) {
			iNodes.put(node, myLayout.createNode(node));
		}
		for (Edge e : edges) {
			Node[] sources = e.getSources();
			INode target = iNodes.get(e.getTarget());
			// Simple edge; no need for grouping
			if (sources.length == 1) {
				String type = e.getWeak()[0] ? "dashed" : "";
				myLayout.addEdge(iNodes.get(sources[0]), target, type);
			}
			// Hyperedge, needs to be grouped
			else {
				INode plus = myLayout.createPlusNode();
				// A hyperedge is always a part of an OR dependency, so it's always weak
				myLayout.addEdge(plus, target, "dashed");
				for (int i = 0; i < sources.length; ++i) {
					String type = e.getWeak()[i] ? "dashed" : "";
					myLayout.addEdge(iNodes.get(sources[i]), plus, type);
				}
			}
		}
		myLayout.layout(graphComponent);
	}
	
	public void layout(GraphComponent graphComponent, String program) {
		MyLayout myLayout = new MyLayout(graphComponent.getGraph());
		Map<Node, INode> iNodes = new HashMap<>();
		for (Node node : nodes) {
			if (Arrays.asList(node.getPrograms()).contains(program)) {
				iNodes.put(node, myLayout.createNode(node, program));
			}
		}
		for (Edge e : edges) {
			if (Arrays.asList(e.getTarget().getPrograms()).contains(program)) {
				Node[] sources = e.getSources();
				INode target = iNodes.get(e.getTarget());
				// Simple edge; no need for grouping
				if (sources.length == 1) {
					String type = e.getWeak()[0] ? "dashed" : "";
					INode source = iNodes.get(sources[0]);
					// the source may not be already in iNodes, if it's external to the program
					if (source == null) {
						source = myLayout.createNode(sources[0], program);
						iNodes.put(sources[0], source);
					}
					myLayout.addEdge(source, target, type);
				}
				// Hyperedge, needs to be grouped
				else {
					INode plus = myLayout.createPlusNode();
					// A hyperedge is always a part of an OR dependency, so it's always weak
					myLayout.addEdge(plus, target, "dashed");
					
					for (int i = 0; i < sources.length; ++i) {
						String type = e.getWeak()[i] ? "dashed" : "";
						INode source = iNodes.get(sources[i]);
						// the source may not be already in iNodes, if it's external to the program
						if (source == null) {
							source = myLayout.createNode(sources[i], program);
							iNodes.put(sources[i], source);
						}
						myLayout.addEdge(source, plus, type);
					}
				}
			}
		}
		myLayout.layout(graphComponent);
	}
}
