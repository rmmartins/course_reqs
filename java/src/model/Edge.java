package model;

public class Edge {
	
	protected Node[] sources;
	protected Boolean[] weak;
	protected Node target;
	
	protected Edge(Node[] sources, Node target, Boolean[] weak) {
		this.sources = sources;
		this.target = target;
		this.weak = weak;
	}
	
	protected Edge(Node source, Node target, boolean weak) {
		this(new Node[] { source }, target, new Boolean[] { weak });
	}

	public Node[] getSources() {
		return sources;
	}

	public Boolean[] getWeak() {
		return weak;
	}

	public Node getTarget() {
		return target;
	}
	
	
}
