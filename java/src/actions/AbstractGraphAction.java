package actions;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import com.yworks.yfiles.view.GraphComponent;

@SuppressWarnings("serial")
public abstract class AbstractGraphAction extends AbstractAction {

	protected GraphComponent graphComponent;
	protected JFileChooser jfc = new JFileChooser();

	private String propFile = ".yFiles.properties";
	private Properties p;

	public AbstractGraphAction(GraphComponent graphComponent, String title) {
		super(title);
		this.graphComponent = graphComponent;
		loadProps();		
	}

	private final void loadProps() {
		try (FileReader reader = new FileReader(propFile)) {
			p = new Properties();
			p.load(reader);
			jfc.setCurrentDirectory(new File(p.getProperty("path")));
		} catch (Exception e) {
			e.getMessage();
		}
	}

	protected final void saveProps() {
		try (FileWriter writer = new FileWriter(propFile)) {			
			p.setProperty("path", jfc.getCurrentDirectory().getAbsolutePath());
			p.store(writer, "");
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
