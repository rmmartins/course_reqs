package actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.yworks.yfiles.view.GraphComponent;

import model.Graph;
import model.Node;

@SuppressWarnings("serial")
public class OpenCSVAction extends AbstractGraphAction {

	private Graph myGraph;

	public OpenCSVAction(GraphComponent graphComponent) {
		super(graphComponent, "Open CSV");		
		jfc.setFileFilter(new FileNameExtensionFilter("CSV file", "csv"));
		jfc.setMultiSelectionEnabled(true);
	}
	
	public void loadCSV(File[] files) {
		myGraph = new Graph();
		
		// This map saves dep. strings while parsing the nodes in step 1
		Map<Node, String> depStrings = new HashMap<>();
		
		for (File f : files) {
			String absPath = f.getAbsolutePath();
			try (Stream<String> stream = Files.lines(Paths.get(absPath))) {			
				String program = absPath.substring(absPath.lastIndexOf("_") + 1).split("\\.")[0];
				// First row is made of headers, so skip it
				stream.skip(1).forEach(row -> {
					// We start by creating all nodes, without parsing dependencies yet 
					String[] cols = row.split(";");
					// the course code (col. 0) must be present, or else the row is invalid 
					if (cols.length > 0 && !cols[0].isEmpty()) {
						String code = cols[0].equals("______") ? "Elective" : cols[0];
						// Year
						int year = Integer.parseInt(cols[4]);
						// Period
						// 15-credits courses have two periods, but only the first is used for now
						String[] periods = cols[5].split("-");
						int period = Integer.parseInt(periods[0]);
						// Optional
						boolean optional = cols.length > 10 && cols[10].trim().equals("x");
						
						Node node = myGraph.createNode(program, code, year, period, optional);
						
						// Save deps for later
						if (cols.length > 6) {
							if (depStrings.get(node) == null) {
								depStrings.put(node, cols[6].trim());
							} else if (!depStrings.get(node).equals(cols[6])) {
								Logger.getGlobal().severe("Inconsistent dependencies for course " + code);
							}
						}
						
						// For 15-credit courses, add a second node depending on the first
						if (periods.length > 1) {
							String code2 = code + "_";
							Node node2 = myGraph.createNode(program, code2, year, Integer.parseInt(periods[1]), optional);
							// TODO just add the edge directly; there are no unknowns dependencies here
							depStrings.put(node2, code);
						}
						
						// cols[7] are CS credit requirements
						if (cols.length > 7 && !cols[7].isEmpty()) {
							node.setReqsCs(Integer.parseInt(cols[7]));							
						}
						
						// cols[8] are 2DVxxxCS credit requirements
						if (cols.length > 8 && !cols[8].isEmpty()) {
							node.setReqs2dv(Integer.parseInt(cols[8]));						
						}
						
						// cols[9] are Other credit requirements
						if (cols.length > 9 && !cols[9].isEmpty()) {
							node.setReqsOther(Integer.parseInt(cols[9]));						
						}
					}
				});
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
				
		// Now that all nodes are in the graph, we parse the dependencies
		for (Entry<Node, String> entry : depStrings.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				Node target = entry.getKey();
				
				String[] orSides = entry.getValue().split("\\|");
				
				// Scenario #1: no OR dependencies
				if (orSides.length == 1) {
					String[] deps = orSides[0].split(","); 
					for (String dep : deps) {
						Node[] depNode = myGraph.getNodesBy(dep, true);
						// The edges are strong, unless it is a duplicated dependency
						for (Node source : depNode) {
							myGraph.createEdge(source, target, depNode.length > 1);
						}
					}
				}
				
				// Scenario #2: there are OR dependencies
				else {
					for (String side : orSides) {
						String[] deps = side.split(",");
						
						// Scenario #2.1: this side of the OR has a single dependency
						if (deps.length == 1) {
							Node[] depNode = myGraph.getNodesBy(deps[0], true);
							// The edges are always weak
							for (Node source : depNode) {
								myGraph.createEdge(source, target, true);
							}
						}
						
						// Scenario #2.2: this side of the OR has multiple dependencies; we need to use hyperedges
						else {
							List<Node> sources = new ArrayList<>();
							List<Boolean> weak = new ArrayList<>();
							for (String dep : deps) {
								Node[] depNode = myGraph.getNodesBy(dep, true);
								// The source-edges are strong, unless it is a duplicated dependency
								for (Node source : depNode) {
									sources.add(source);
									weak.add(depNode.length > 1);
								}								
							}
							myGraph.createEdge(sources.toArray(new Node[0]), target, weak.toArray(new Boolean[0]));
						}
					}
				}
				
			}
		}				
	}

	@Override
	public void actionPerformed(ActionEvent e) {		
		if (jfc.showOpenDialog(graphComponent) == JFileChooser.APPROVE_OPTION) {
			File[] files = jfc.getSelectedFiles();
			loadCSV(files);
			myGraph.layout(graphComponent);
//			myGraph.layout(graphComponent, "WP");
		}
		saveProps();
	}

	public Graph getMyGraph() {
		return myGraph;
	}

}
