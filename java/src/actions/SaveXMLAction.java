package actions;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.yworks.yfiles.view.GraphComponent;

@SuppressWarnings("serial")
public class SaveXMLAction extends AbstractGraphAction {

	public SaveXMLAction(GraphComponent graphComponent) {
		super(graphComponent, "Save XML");
		jfc.setFileFilter(new FileNameExtensionFilter("XML file", "xml"));
	}
	
	public void saveToXML(String absPath) {
		try {
			graphComponent.exportToGraphML(absPath);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (jfc.showSaveDialog(graphComponent) == JFileChooser.APPROVE_OPTION) {
			String absPath = jfc.getSelectedFile().getAbsolutePath();
			saveToXML(absPath);
		}
		saveProps();
	}
}