
function mergePrograms(program, prefix, courses) {
	for (let course of program) {
		let code = course["Course code"];
		if (code.length > 0) {
			// Assemble the key
			code = (code == "______") ? "Elective" : code;			
			const year = course["year"];			
			let period = course["Study period"].split('-')[0];
			const key = `${code}.${year}.${period}`;

			let opt = course["Optional course"] == "x";

			// Add the course if it is not yet in the map
			if (!courses.has(key)) {
				courses.set(key, course);
				course.programs = new Set();
				course.optional = new Set();
			}

			// and update it as necessary
			course = courses.get(key);
			course.programs.add(prefix);
			
			if (opt)
				course.optional.add(prefix);
		}
	}
}
