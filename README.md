# README #

This is a course-dependencies graph developed for Linnaeus University's Computer Science Department.

### Requirements ###

This program depends on the [yFiles for Java](https://www.yworks.com/products/yfiles-for-java) library. Unfortunately it is not free, but apparently there are no other graph drawing libraries quite like it out there.

### Development and Usage ###

The program consists on two main parts: the Java backend, where the graph layout is generated; and the JavaScript frontend, where the graph is presented to the users.

#### The Java backend #### 

The subfolder *java* is an Eclipse project. It depends on *yFiles*, as mentioned above, so make sure you have yFiles setup as a User Library. There is a crude GUI on *main.Main*, but it is currently undocumented. In order to use the CLI (as described below), export the project as a *Runnable JAR file*, with *main.CmdLine* as its main class, and make sure you choose *Extract required libraries into generated JAR* (tested with Eclipse Oxygen, 03/2018).

Supposing you exported your JAR file as `courseDeps.jar`, you can run the following command to get the CLI instructions:
````
java -jar courseDeps.jar
````

Please look at one of the examples in the *input* folder for the input file format.

#### The JavaScript frontend ####

In order to see the results, simply put the application in an HTTP server and open *index.html*. The list of graphs to be shown is currently hardcoded into *index.html* (yes, I know), so add a new one if necessary.

### Who do I talk to? ###

Rafael M. Martins, rafael.martins@lnu.se