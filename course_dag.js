
function prepareGraph(data_xml, rows, program) {

  // Convert the XML document to an array of objects.
  // Note that querySelectorAll returns a NodeList, not a proper Array,
  // so we must use map.call to invoke array methods.
  var nodes = {};
  data_xml.querySelectorAll("node").forEach(function(node) {
    
    const label = node.querySelector("y\\:Label\\.Text, Label\\.Text").textContent;
    let code = label;
    if (node.querySelector("sys\\:String, String")) {
      code = node.querySelector("sys\\:String, String").textContent;
    }

    // Missing nodes get an empty row
    if (!rows.has(code)) {
      rows.set(code, {});
    }

    const thisCourse = rows.get(code);

    // Bounds
    var rect = node.querySelector("y\\:RectD, RectD");

    // Ports
    var ports = {};
    node.querySelectorAll("port").forEach(function(port) {
      ports[port.getAttribute("name")] =
          port.querySelector("yfj\\:FreeNodePortLocationModelParameter, " +
                    "FreeNodePortLocationModelParameter").getAttribute("Ratio")
                    .split(",");
    });

    // Width may vary
    let width = +rect.getAttribute("Width");

    // Period may include more than one number
    let period = 1;
    if (thisCourse["Study period"]) {
      period = +thisCourse["Study period"].split("-")[0];
    }

    // TODO Maybe build this object in the backend, and send this back as a JSON object attached to the node?
    // TODO Is this even necessary? Can't I just use the already existing node?

    if (!label.endsWith("_") || label == "______") {

      nodes[node.getAttribute("id")] = {
        label:              label == "______" ? "Elective" : label,
        x:                  +rect.getAttribute("X"),
        y:                  +rect.getAttribute("Y"),
        width:              width,
        height:             +rect.getAttribute("Height"),
        ports:              ports,
        name:               thisCourse["Name"],
        credits:            +thisCourse["Credits"],
        year:               +thisCourse["year"],
        period:             period,
        reqs:               thisCourse["Course requirements"],
        cred_reqs_cs:       thisCourse["Credit requirements CS"],
        cred_reqs_2dv:      thisCourse["Credit requirements 2DVxxx CS"],
        cred_reqs_other:    thisCourse["Credit requirements other"],
        prefix:             thisCourse.programs,
        optional:           thisCourse.optional
      };

    } else {
      // A node ending in _ means it's a multi-period node, so we enlarge the original node instead
      for (let n of Object.values(nodes)) {
        if (n.label == label.substr(0, label.length - 1)) {
          n.width = +rect.getAttribute("X") + width - n.x;
          break;
        }
      }
    }

  });

  var edges = [].filter.call(data_xml.querySelectorAll("edge"), function(edge) {
    // last coordinate comes from target port
    var target = nodes[edge.getAttribute("target")];
    // first coordinate comes from source port
    var source = nodes[edge.getAttribute("source")];

    return (source && target);
  });

  edges = edges.map(function(edge) {
    // last coordinate comes from target port
    var target = nodes[edge.getAttribute("target")];
    // first coordinate comes from source port
    var source = nodes[edge.getAttribute("source")];

    var bends = [];

    var sourceport = source.ports[edge.getAttribute("sourceport")];
    bends.push([
        (sourceport[0] == "0" ? source.x : source.x + source.width),
        source.y + source.height * +sourceport[1]
    ]);
    // intermediate coordinates come from bends
    edge.querySelectorAll("y\\:Bend, Bend").forEach(function(bend) {
        bends.push(bend.getAttribute("Location").split(","));
    });

    var targetport = target.ports[edge.getAttribute("targetport")];
    bends.push([
        (targetport[0] == "0" ? target.x - 3 : target.x + target.width + 3),
        target.y + target.height * +targetport[1]
    ]);
    // Type differs between dashed or full stroke
    let edge_type_sel = edge.querySelector("sys\\:String, String");
    let type = edge_type_sel ? edge_type_sel.innerHTML : "";
    
    return {
      bends: bends,
      source: source,
      target: target,
      type: type
    }
  });

  return createGraph(nodes, edges, program);
}


function createGraph(nodes, edges, program) {
  // All elements will be under this group
  const svgGroup = svg.append("g");

  var nodes = d3.values(nodes);

  const xExt = d3.extent(nodes, d => d.x);
  xExt[1] = d3.max(nodes, d => d.x + d.width);

  let yExt = d3.extent(nodes, d => d.y);

  // ---------- GENERAL SETUP ----------

  var min_y = d3.min(nodes.map(function(d) { return d["y"]; }));
  var max_x = d3.max(nodes.map(function(d) { return d["x"]; }));
  // let width, height;

  function scaleSvg() {
    const totalWidth = xExt[1] - xExt[0];
    let curWidth = document.body.clientWidth;
    var wScale = curWidth / totalWidth;

    // Add a bit of room for the legend at the bottom
    const totalHeight = yExt[1] - yExt[0] + 130;
    // let curHeight = document.body.scrollHeight;
    let curHeight = window.innerHeight;
    var hScale = curHeight / totalHeight;

    // console.log(wScale, hScale);

    const scale = Math.min(wScale, hScale) * 0.9;

    var trans_y = (-min_y + 10) * scale;
    var translateX = 0;
    let margin = (curWidth - (totalWidth + nodes[0].width + 20) * scale) / 2;
    let transforms = [
      "translate(" + margin + "," + trans_y + ")",
      "scale(" + scale + ")"
    ];
    svgGroup.attr("transform", transforms.join(" "));

    svgGroup._height = trans_y + 50;
    // svg.attr("height", svgGroup._height);

    svgGroup._width = totalWidth;
    // svg.attr("width", svgGroup._width);
  }

  scaleSvg();

  // Setup a custom colormap
  let cmap1 = d3.scale.category20b().range();
  let cmap2 = d3.scale.category20c().range();
  cmap2 = cmap2.slice(0, -4).concat(cmap1.slice(12, 16));
  let colormap = cmap2.map((c, i) => cmap2[Math.floor(i / 4) * 4 + 3 - (i % 4)])

  let lineFunction = d3.svg.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .interpolate("linear");

  window.onresize = function() {
    scaleSvg();
  }

  // ---------- LEGEND ----------

  const defs = svg.append("defs");

  // Find out how many years the course has
  const num_years = Math.max(...nodes.filter(n => !isNaN(n.year)).map(n => n.year));

  const year_bounds = [];
  // Add one entry per year
  for (let i = 0; i < num_years; ++i) {
    year_bounds.push([svgGroup._width, 0]);
  }

  nodes.forEach(function(n) {
    if (n.year) {
      year_bounds[n.year - 1][0] = Math.min(n.x, year_bounds[n.year - 1][0]);
      year_bounds[n.year - 1][1] = Math.max(n.x + n.width, year_bounds[n.year - 1][1]);
    }
  });

  let h = 0;

  for (let i = 0; i < num_years; ++i) {
    let gradient = defs.append("linearGradient")
        .attr("id", "gradient-year" + (i + 1));
    
    gradient.append("stop")
        .attr("offset", "0")
        .attr("stop-color", colormap[i * 4]);

    gradient.append("stop")
        .attr("offset", "1")
        .attr("stop-color", colormap[i * 4 + 3]);

    let x = year_bounds[i][0] - h + 5;
    if (i < num_years - 1)
      h = (year_bounds[i + 1][0] - year_bounds[i][1]) / 2;
    let w = year_bounds[i][1] - year_bounds[i][0] + h;

    // var w = year_bounds[2][1] - year_bounds[2][0] + h + 10;

    svgGroup.append("rect")
        .attr("class", "legend")
        .attr("x", x)
        .attr("y", 20)
        .attr("width", w)
        .attr("height", 20)
        .attr("fill", "url(#gradient-year" + (i + 1) + ")");

    svgGroup.append("text")
        .text("Year " + (i + 1))
        .attr("x", x + w / 2)
        .attr("y", 30);
  }

  // ---------- EDGES ----------

  defs.append("marker")
        .attr({
          "id":"arrow",
          "viewBox":"0 -5 10 10",
          "refX":5,
          "refY":0,
          "markerWidth":4,
          "markerHeight":4,
          "orient":"auto"
        })
        .append("path")
          .attr("d", "M0,-5L10,0L0,5")
          .attr("class","arrowHead");

  var paths_sel = svgGroup.selectAll("path")
        .data(edges)
      .enter().append("path")
      .attr("d", function(d) {
          return lineFunction(d.bends);
      })
      .attr("stroke-dasharray", d => d.type == "dashed" ? "5 5" : "")
      .attr("class", "edge");

// ---------- NODES ----------

  var nodes_g = svgGroup.selectAll("g.node")
      .data(nodes)
    .enter().append("g")
      .attr("class", "node");

  var rects = nodes_g.filter(d => !d.label.includes(":"))
      .append("rect")
          .attr("x", function(d) { return d.x; })
          .attr("y", function(d) { return d.y; })
          .attr("width", function(d) { return d.width; })
          .attr("height", function(d) { return d.height; })
          .attr("rx", 5)
          .attr("ry", 5)
          .attr("stroke-dasharray", d => (d.optional && d.optional.has(program)) ? "5 5" : "")
          .attr("fill", d => d.name ? colormap[(d.year - 1) * 4 + d.period - 1] : "lightgray");

  nodes_g.filter(d => d.label.includes(":"))
      .append("ellipse")
          .attr("cx", function(d) { return d.x + d.width / 2; })
          .attr("cy", function(d) { return d.y + d.height / 2; })
          .attr("rx", function(d) { return d.width / 2; })
          .attr("ry", function(d) { return d.height / 2; })
          .attr("fill", "lightgray");

  var text_sel = nodes_g.append("text")
      .attr("x", d => d.x + d.width / 2)
      .attr("y", d => d.y + d.height / 2 - (d.label.length == 1 ? 3 : 0))
      .style("font-weight", d => d.label.length == 1 ? "bold" : "normal")
      .style("font-size", d => d.label.length == 1 ? "xx-large" : "normal")
      .text(d => d.label);

  let hasPrefix = nodes_g.filter(d => d.name && d.prefix.size > 0);

  let r = 10;
  hasPrefix.each(function(node) {
    let prefixes = [...node.prefix];
    for (let i = 0; i < node.prefix.size; ++i) {
      let x = node.x + node.width - (r * 2 + 2) * i - r - 2;
      // let x = node.x + (r * 2 + 2) * i + r + 2;
      let y = node.y;

      d3.select(this).append("circle")
          .attr("class", "prefix")
          .attr("r", r)
          .attr("cx", x)
          .attr("cy", y);

      d3.select(this).append("text")
          .attr("class", "prefix")
          .attr("x", d => x)
          .attr("y", d => y)
          .text(d => prefixes[i]);
    }
  })

  // ---------- EVENTS ----------

  $("g.node").tipsy({
    gravity: function() {
        g = '';
        if (this.__data__.y > (yExt[0] + yExt[1]) / 2) {
          g += 's';
        } else {
          g += 'n';
        }
        if (this.__data__.x > (xExt[0] + xExt[1]) / 2) {
          g += 'e';
        } else {
          g += 'w';
        }
        return g;
      },
    html: true,
    opacity: 1.0,
    title: function() {
      var tooltip = "";
      var d = this.__data__;
      if (d.name) {
        tooltip = "<p class=name>" + d.name + "</p>" +
            "<p class=year><span>Year:</span> " + d.year + ", " +
            "<span>Period:</span> " + d.period + "</p>";
        var reqs = [];
        if (d.cred_reqs_cs)
          reqs.push("CS: " + d.cred_reqs_cs);
        if (d.cred_reqs_2dv)
          reqs.push("2DVxxx: " + d.cred_reqs_cs);
        if (d.cred_reqs_other)
          reqs.push("Other: " + d.cred_reqs_other);
        if (reqs.length > 0) {
          tooltip += "<p class=reqs><span>Requirements</span><br>";
          tooltip += reqs.join(", ");
          tooltip += "</p>";
        }
      }
      return tooltip;
    }
  });

  svgGroup.clearSelection = function() {
    nodes_g.style("opacity", 1);
    paths_sel.style("opacity", 1);
  }

  nodes_g.on("click", function(node) {
    // Start on a clean slate
    svgGroup.clearSelection();
    var show_nodes = [node];
    paths_sel.each(function(edge) {
      if (edge.source == node || edge.target == node) {
        d3.select(this).attr("style", "opacity: 1");
        show_nodes.push(edge.source);
        show_nodes.push(edge.target);
      }
    });
    show_nodes = [...new Set(show_nodes)];
    selectNodes(show_nodes);
    d3.event.stopPropagation();
  });

  nodes_g.on("dblclick", function(node) {
    window.open(`http://kursplan.lnu.se/kursplaner/syllabus-${node.label}.pdf`, '_blank');
  });

  // TODO this function looks ugly...
  function selectNodes(nodes) {
    nodes_g.attr("style", "opacity: 0.2");
    paths_sel.attr("style", "opacity: 0.2");

    nodes_g.filter(function(node) {
      return nodes.includes(node);
    }).attr("style", "opacity: 1");

    paths_sel.each(function(edge) {
      if (nodes.includes(edge.source) && nodes.includes(edge.target)) {
        d3.select(this).attr("style", "opacity: 1");
      }
    });
  }

  return svgGroup;

}
